# variables.tf
variable "unique_token" {
  description = "A unique token for naming resources"
  type        = string
}

