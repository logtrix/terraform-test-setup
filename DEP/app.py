# dep_app.py
from flask import Flask, request

app = Flask(__name__)
status = "pang"

@app.route('/status')
def get_status():
    return status

@app.route('/set_status/<new_status>', methods=['POST'])
def set_status(new_status):
    global status
    status = new_status
    print(f"Status set to: {new_status}")
    return ""


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

