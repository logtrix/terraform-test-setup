# main.tf

terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
  }
}

provider "docker" {
  host    = "unix:///var/run/docker.sock"
}

### PRIVATE NETWORK TO LINK CONTAINERS
resource "docker_network" "private" {
  name = "private_network-${var.unique_token}"
}

### TEST SERVICES
resource "docker_container" "dep" {
  name  = "dep-${var.unique_token}"
  image = "${var.unique_token}-dep"
  networks_advanced {
    name = docker_network.private.name
  }
}

### SOFTWARE UNDER TEST
resource "docker_container" "sut" {
  name  = "sut-${var.unique_token}"
  image = "${var.unique_token}-sut"
  networks_advanced {
    name = docker_network.private.name
  }
}

### THE TEST HARNESS
#resource "docker_container" "test" {
#  name  = "test-${var.unique_token}"
#  image = "${var.unique_token}-test"
#  networks_advanced {
#    name = docker_network.private.name
#  }
#}

