#!/bin/bash
set -e
# This script builds and tags containers, launches them in Terraform with a private network

# Set unique token
UNIQUE_TOKEN=12345

# Build and tag SUT container
echo ":::: BUILDING : SUT"
docker build -t "${UNIQUE_TOKEN}-sut" --build-arg UNIQUE_TOKEN="${UNIQUE_TOKEN}" -f SUT/Dockerfile SUT

# Build and tag TEST container
echo ":::: BUILDING : TEST"
docker build -t "${UNIQUE_TOKEN}-test" --build-arg UNIQUE_TOKEN="${UNIQUE_TOKEN}" -f TEST/Dockerfile TEST

# Build and tag DEP container
echo ":::: BUILDING : DEP"
docker build -t "${UNIQUE_TOKEN}-dep" --build-arg UNIQUE_TOKEN="${UNIQUE_TOKEN}" -f DEP/Dockerfile DEP

# Initialize Terraform
echo ":::: TERRAFORM INIT"
terraform init

# Apply Terraform configuration
echo ":::: TERRAFORM APPLY"
terraform apply -var="unique_token=${UNIQUE_TOKEN}" -auto-approve

