#!/bin/bash

# This script tears down the Terraform instance created by before_script
# and untags the containers

# Set unique token
UNIQUE_TOKEN=12345

docker logs sut-${UNIQUE_TOKEN} > sut.log
docker logs dep-${UNIQUE_TOKEN} > dep.log
docker logs test-${UNIQUE_TOKEN} > test.log

# Destroy Test instance
docker rm "test-${UNIQUE_TOKEN}"

# Destroy Terraform resources
terraform destroy -var="unique_token=${UNIQUE_TOKEN}" -auto-approve

# Remove container tags
docker rmi "${UNIQUE_TOKEN}-sut" "${UNIQUE_TOKEN}-test" "${UNIQUE_TOKEN}-dep"

