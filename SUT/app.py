# app.py
from flask import Flask
import requests
import os

app = Flask(__name__)

dep_url = f"http://DEP-{os.environ['UNIQUE_TOKEN']}:5000/status"

@app.route('/ping')
def ping():
    return get_dep_status()

def get_dep_status():
    try:
        response = requests.get(dep_url, timeout=2)
        return response.text
    except requests.exceptions.RequestException:
        return 'DEP not reachable'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

