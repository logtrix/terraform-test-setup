# test_app.py
import requests
import pytest
import os

@pytest.fixture
def set_dep_status_pong():
    # Make a REST call to DEP to set its expected response to "pong"
    dep_url = f"http://DEP-{os.environ['UNIQUE_TOKEN']}:5000/set_status/pong"
    response = requests.post(dep_url)
    assert response.status_code == 200

@pytest.fixture
def set_dep_status_pang():
    # Make a REST call to DEP to set its expected response to "pang"
    dep_url = f"http://DEP-{os.environ['UNIQUE_TOKEN']}:5000/set_status/pang"
    response = requests.post(dep_url)
    assert response.status_code == 200

def test_ping_with_dep_status_pong(set_dep_status_pong):
    response = requests.get(f"http://SUT-{os.environ['UNIQUE_TOKEN']}:5000/ping")
    assert response.status_code == 200
    assert response.text == 'pong'

def test_ping_with_dep_status_pang(set_dep_status_pang):
    response = requests.get(f"http://SUT-{os.environ['UNIQUE_TOKEN']}:5000/ping")
    assert response.status_code == 200
    assert response.text == 'pang'

